$(document).ready(function(){

  var key = 'vatnajokull';

  var bandcamp_url = getURLParameter('url');

  if (bandcamp_url != 'null') { load_artist(bandcamp_url); }

  $("#obtener_enlaces").click(function() { load_artist($('.url_album').val()); });

  function load_artist (url) {
    $.ajax({
      'url': 'http://api.bandcamp.com/api/url/1/info?key=' + key + '&url=' + url,
      'type': "GET",
      'dataType': 'jsonp',
      beforeSend: function () {
        $('#obtener_enlaces').button('loading');
        $("#loading").fadeIn();
      },
      success: function(res) {
        if (!res.error && res.album_id) {
          load_tracks(res.album_id);
        } else {
          clear();
          error();
        }
      },
      error: function () {
        error();
      }
    });
  }
  
  function load_tracks (album_id) {
    $.ajax({
      'url': 'http://api.bandcamp.com/api/album/2/info?key=' + key + '&album_id=' + album_id,
      'type': "GET",
      'dataType': 'jsonp',
      success: function(res) {
          clear();
          $('#tracks').append('<tr id="cabecera"></tr>');
          $('#cabecera').append('<th>#</th>');
          $('#cabecera').append('<th>Nombre canci&oacute;n</th>');
          $('#cabecera').append('<th>Link de descarga</th>');
          $('#nombre').append('<h3>' + res.title + '</h3>');
        for(i = 0 ; i < res.tracks.length ; i++) {
          $("#tracks").fadeIn();
          $('#tracks').append('<tr id="t-' + i + '"></tr>');
          $('#t-' + i).append('<td>'+ res.tracks[i].number +'</td>');
          $('#t-' + i).append('<td>'+ res.tracks[i].title +'</td>');
          $('#t-' + i).append('<td><a href="'+ res.tracks[i].streaming_url +'">Guardar enlace como...</a></td>');
        }
        $('#obtener_enlaces').button('reset');
      },
      error: function () {
        error();
      }
    });
  }

  function clear () {
    $('#tracks').empty();
    $('#mensaje').empty();
    $('#nombre').empty();
  }

  function error () {
    $('#obtener_enlaces').button('reset');
    $('#mensaje').append(true, '<br><div class="alert alert-error">Error, verifique que la direccion del &aacute;lbum sea correcta</div>');
  }

  function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    );
  }

});